Drawing a Line in OpenGL and Bresenham Line Algorithm. Implemented in Python using pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "2. VJEZBA."

Created: 2020

from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse

# ------------ svi stupnjevi - CIJELI brojevi ------------

#Lx, Ly
# Ix
Lx, Ly = [None, None], [None, None]
Ix = None

width = 300
height = 300

# otvara prozor
config = pyglet.gl.Config(double_buffer=False)
window = pyglet.window.Window(width=width, height=height, caption="Bresenham", resizable=True, config=config)
window.set_location(100, 100)


def myLine2(xa, ya, xb, yb):
    # crtanje gotove linije
    glBegin(GL_LINES)
    glVertex2i(xa, ya + 20)
    glVertex2i(xb, yb + 20)
    glEnd()

    # Bresenham
    if yb-ya <= xb-xa:
        x = xa
        y = ya

        dx = xb - xa
        if dx == 0:
            return
        dy = yb - ya

        a = 2 * dy
        correction = -2 * dx

        d = a - dx

        glBegin(GL_POINTS)
        for i in range(dx + 1):
            glVertex2i(x, y)
            if d > 0:
                y += 1
                d = d + correction
            x += 1
            d += a
        glEnd()
    else:
        xa, ya = ya, xa
        xb, yb = yb, xb

        x = xa
        y = ya

        dx = xb - xa
        if dx == 0:
            return
        dy = yb - ya

        a = 2 * dy
        correction = -2 * dx

        d = a - dx

        glBegin(GL_POINTS)
        for i in range(dx + 1):
            glVertex2i(y, x)
            if d > 0:
                y += 1
                d = d + correction
            x += 1
            d += a
        glEnd()

def myLine3(xa, ya, xb, yb):
    # crtanje gotove linije
    glBegin(GL_LINES)
    glVertex2i(xa, ya + 20)
    glVertex2i(xb, yb + 20)
    glEnd()

    # Bresenham
    if -(yb-ya) <= xb-xa:
        x = xa
        y = ya

        dx = xb - xa
        if dx == 0:
            return
        dy = yb - ya

        a = 2 * dy
        correction = 2 * dx

        d = a + dx

        glBegin(GL_POINTS)
        for i in range(dx + 1):
            glVertex2i(x, y)
            if d < 0:
                y -= 1
                d = d + correction
            x += 1
            d += a
        glEnd()
    else:
        xa, yb = yb, xa
        xb, ya = ya, xb

        x = xa
        y = ya

        dx = xb - xa
        if dx == 0:
            return
        dy = yb - ya

        a = 2 * dy
        correction = 2 * dx

        d = a + dx

        glBegin(GL_POINTS)
        for i in range(dx + 1):
            glVertex2i(y, x)
            if d < 0:
                y -= 1
                d = d + correction
            x += 1
            d += a
        glEnd()


def myLine(xa, ya, xb, yb):
    if xa <= xb:
        if ya <= yb:
            myLine2(xa, ya, xb, yb)
        else:
            myLine3(xa, ya, xb, yb)
    else:
        if ya >= yb:
            myLine2(xb, yb, xa, ya)
        else:
            myLine3(xb, yb, xa, ya)

#+
@window.event
def on_mouse_press(x, y, button, modifiers):
    global Lx, Ly, Ix

    if button & mouse.LEFT:

        Lx[Ix] = x
        Ly[Ix] = y
        Ix = Ix ^ 1

        if Ix == 0:
            myLine(Lx[0], Ly[0], Lx[1], Ly[1])
        else:
            glVertex2i(x, y)

        print("Koordinate tocke {}: {} {} ".format(Ix ^ 1, x, y))

        glFlush()

    elif button == mouse.RIGHT:
        on_resize(width, height)


#+
@window.event
def on_key_press(symbol, modifiers):
    if symbol == key.R:
        glColor3f(1, 0, 0)
    elif symbol == key.G:
        glColor3f(0, 1, 0)
    elif symbol == key.B:
        glColor3f(0, 0, 1)
    elif symbol == key.K:
        glColor3f(0, 0, 0)

    print("w ",width, ", h",height)
    glRecti(width - 15, height - 15, width, height)
    glFlush()

#+
@window.event
def on_draw():
    #glClearColor(1.0, 1.0, 1.0, 1.0)
    #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glFlush()

#+
@window.event
def on_resize(w, h):
    print("resize!!")
    global width, height
    width = w
    height = h
    global Ix
    Ix = 0
    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(1.0)
    glColor3f(0.0, 0.0, 0.0)





if __name__ == "__main__":
    print("Lijevom tipkom misa zadaj tocke - algoritam Bresenham-a")
    print("Tipke r, g, b, k mijenjaju boju.")

    pyglet.app.run()